/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function() {

  let form = $('form#customer-form')[0];
  if (form!=null){
    isValidPartitaIVA(form);
  }

  $('#customer-form .form-control-submit').on('click',function (e) {
    isValidPartitaIVA(this.form);
  });
  //jQuery  1.7 >= 1.4.3 (Registra il evento anche se l'elemento non esiste ancora)
  $('#customer-form').delegate(".form-control-submit", "click", function(){
    isValidPartitaIVA(this.form);
  });



});

function isValidPartitaIVA(form) {

  let fields = form.querySelector('fieldset');
  if (fields != null){
    let elements = fields.children;

    $(elements).each(function (index) {

      let label = this.querySelector('label');
      if (label != null){
        //get this element input and verify that length == 11
        if (label.innerText === "Partita IVA" || label.innerText.includes('Partita IVA')){
          let p_iva = this.querySelector('input');

          let select = elements[(index+2)].querySelector('select');
          if (select.value === 'Yes'){
            p_iva.required = true;
          }else {
            p_iva.required = false;
          }
          p_iva.setCustomValidity("");

          p_iva.type = 'number';
          if (!p_iva.checkValidity() || (p_iva.value.length >0 && p_iva.value.length != 11)){
            //p_iva.style.border = '1px solid red';
            p_iva.setCustomValidity("Inserisci un numero di Partita IVA valido");
            return false;
          }else {
            p_iva.setCustomValidity("");
          }
        }
      }

    });
  }
  return true;
}